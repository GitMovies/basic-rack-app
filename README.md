# A Bare Bones Rack App

## What Is Rack?
Rack, at its most basic, sits between a ruby webserver, such as Puma, and a web
application, such as the one in this repo. Rack provides an interface for
abstracting away the finer details of HTTP request handling. All you need to
adhere to the Rack interface and you're good to go.

## Try It
1. `gem install rack`
2. `gem install puma`
3. `rackup config.ru`
4. `curl http://localhost:[port puma is running on]`
  * After running the `rackup` command the resulting output will show the port
    that's being listened on. This is what you'll want to run curl against.
