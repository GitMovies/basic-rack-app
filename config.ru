class TimestampMiddleware
  def initialize(app)
    @app = app
  end

  def call(env)
    env['timestamp'] = Time.now.iso8601
    @app.call(env)
  end
end

class RackEmUp
  def call(env)
    [200, {'Content-Type'=>'text/html'}, ["hello! #{env['timestamp']}"]]
  end
end

use TimestampMiddleware
run RackEmUp.new
